import random as rdm
import matplotlib.image as mpimg
import numpy as np
import matplotlib.pyplot as plt
import time


#légende :
# 0 --> plaines
# 1 --> eau
# 2 --> forêt
# 3 --> pierre
# 4 --> maison
# 5 --> ruine
# 6 --> chateau
# 7 --> avant poste
# 8 --> chemin
# 9 --> pont
# 10 --> chemin forestier
# 11 --> chemin de montagne

def distance(x,y,element,terrain,borne):
    d,largeur,longueur = 1,len(terrain),len(terrain[0])
    while d <= max(min(largeur,longueur),borne):
        for i in range( x-d , 1+d+x):
            for j in range( y-d , 1+d+y ):
                if i >= 0 and i < largeur and j >= 0 and j < longueur:
                    if terrain[i][j] == element and (i,j) != (x,y):
                        return d
        d += 1
    return borne +1


# def parcours(aux,i,j,terrain,gpe,visite):
#     if aux == []:
#         return 0
#     coord = aux[-1]
#     visite[i][j] = True
#     aux.pop()
#     return 1 + explore(coord[0],coord[1],terrain,visite,gpe) + parcours(aux,i,j,terrain,gpe,visite)

# def explore(i,j,terrain,visite,gpe):
#     gpe.append([i,j])
#     aux = adjacent(i,j,terrain,visite)
#     return parcours(aux,i,j,terrain,gpe,visite)
        

# def groupes(terrain,taille):
#     res = []
#     visite = []
#     for i in range(len(terrain)):
#         ligne = []
#         for j in range(len(terrain[0])):
#             ligne.append(False)
#         visite.append(ligne)
#     for i in range(len(terrain)):
#         for j in range(len(terrain[0])):
#             if terrain[i][j] in [4,5] and not visite[i][j]:
#                 gpe = []
#                 taille_gpe = explore(i,j,terrain,visite,gpe)
#                 if taille_gpe >= taille:
#                     res.append(gpe)
#     return res


# def adjacent(x,y,terrain,visite):
#     res,largeur,longueur = [],len(terrain),len(terrain[0])
#     for i in range( x -1 , 2+ x ):
#         for j in range( y -1 , 2+y ):
#             if i >= 0 and j >= 0 and i < largeur and j < longueur:
#                 if not visite[i][j] and terrain[i][j] in [4,5] and (i,j) != (x,y):
#                     res.append([i,j])
#     return res


def voisins(x,y,element,terrain):
    compteur,largeur,longueur = 0,len(terrain),len(terrain[0])
    for i in range( x -1 , 2+ x ):
        for j in range( y -1 , 2+y ):
            if i >= 0 and j >= 0 and i < largeur and j < longueur:
                if terrain[i][j] == element and (i,j) != (x,y):
                    compteur += 1
    return compteur


def plus_proche(x,y,element,terrain):
    d,largeur,longueur = 0,len(terrain),len(terrain[0])
    while d <= min(largeur,longueur):
        for i in range( x-d , 1+d+x ):
            for j in range( y-d , 1+d+y ):
                if i >= 0 and j >= 0 and i < largeur and j < longueur:
                    if terrain[i][j] == element:
                        return (i,j)
        d += 1
    return (-1,-1)


def extension(el,x,y,terrain,nbe):
    if nbe <= 0:
        return terrain
    largeur,longueur = len(terrain),len(terrain[0])
    i,j = rdm.randint(-1,1),rdm.randint(-1,1)
    if x +i >= 0 and y+j >= 0 and x+i < largeur and y+j < longueur:
        terrain[x+i][y+j] = el
        return extension(el,x+i,y+j,terrain,nbe-1)
    return extension(el,x,y,terrain,nbe-1)

def map_gen(largeur,longueur,points):
    res = []
    for i in range(largeur):
        ligne = []
        for j in range(longueur):
            ligne.append(0)
        res.append(ligne)
    for k1 in range(points[1][0]):
        x,y = rdm.randint(0,largeur-1),rdm.randint(0,longueur-1)
        res[x][y] = 2 # on place les premiers arbres
        nbe = rdm.randint(points[1][1],points[1][2])
        extension(2,x,y,res,nbe)
    river = []
    for k2 in range(points[0][0]):
        x = rdm.randint(0,largeur-1)
        y = rdm.randint(0,longueur-1)
        res[x][y] = 1 # on place le première case d'eau
        river.append((x,y))
        nbe = rdm.randint(points[0][1],points[0][2])
        extension(1,x,y,res,nbe)
    for k3 in range(points[2][0]):
        x,y = rdm.randint(0,largeur-1),rdm.randint(0,longueur-1)
        res[x][y] = 3
        nbe = rdm.randint(points[2][1],points[2][2])
        extension(3,x,y,res,nbe)
    for l in range(1,len(river[points[3]])):
        pif = rdm.randint(0,len(river[points[3]]))
        x1,y1 = river[pif-1]
        x2,y2 = river[pif]
        creation_riviere(x1,y1,x2,y2,res)
    return res

def creation_riviere(x1,y1,x2,y2,res):
    x_haut, x_bas, y_haut, y_bas = max(x1,x2), min(x1,x2), max(y1,y2), min(y1,y2)
    i,j = x_bas,y_bas
    while i < x_haut and j < y_haut:
        res[i][j] = 1
        choix = rdm.randint(0,1)
        if choix == 0:
            i += 1
        else:
            j += 1

# def epicentre(points):
#     moyenne,s = [0,0],len(points)
#     for pt in points:
#         moyenne[0],moyenne[1] = (moyenne[0]+pt[0])/s,(moyenne[1]+pt[1])/s
#     return moyenne

# def tracer_chemin(village1,village2,terrain):
#     x1,y1 = epicentre(village1)
#     x2,y2 = epicentre(village2)
#     x_haut, x_bas, y_haut, y_bas = int(max(x1,x2)), int(min(x1,x2)), int(max(y1,y2)), int(min(y1,y2))
#     i,j = x_bas,y_bas
#     while i < x_haut and j < y_haut:
#         if  terrain[i][j] < 4:
#             terrain[i][j] += 8
#         choix = rdm.randint(0,1)
#         if choix == 0:
#             i += 1
#         else:
#             j += 1


def evolve(terrain,pourcentage):
    res = []
    for k in range(len(terrain)):
        res.append(terrain[k][:])
    indice = 0
    while indice < pourcentage[0]:
        i,j = rdm.randint(0,len(terrain)-1),rdm.randint(0,len(terrain[0])-1)
        if terrain[i][j] == 0:
            if distance(i,j,1,terrain,2) in [1,2] and distance(i,j,2,terrain,2) in [1,2]:
                res[i][j] = 4 # on place une maison
        if terrain[i][j] in [4,3] and distance(i,j,3,res,6) in [1,2,3,4,5,6] and voisins(i,j,4,terrain) + voisins(i,j,6,terrain) >3:
            (p,q) = plus_proche(i,j,3,res)
            res[p][q] = 0 # on utilise la pierre                
            res[i][j] = 6 #on place un fort
        if terrain[i][j] == 0 and distance(i,j,2,res,2) in [1,2] and distance(i,j,4,res,3) == 3:
            res[i][j] = 7
        indice += 1
    indice = 0
    for i in range(len(terrain)):
        for j in range(len(terrain[0])):
            if res[i][j] == 0 and voisins(i,j,2,terrain) > 2 and rdm.random() < 0.7:
                res[i][j] = 2
            if res[i][j] == 0 and voisins(i,j,2,terrain) > 0 and rdm.random() < 0.1:
                res[i][j] = 2
            if terrain[i][j] == 7 and ( not distance(i,j,4,res,4) in [2,3,4] or not distance(i,j,2,res,2) in [1,2] ):
                res[i][j] = 0
            if terrain[i][j] == 4:
                if distance(i,j,2,terrain,5) > 4:
                    if not distance(i,j,7,res,4) in [2,3,4]:
                        res[i][j] = 5
                    else:
                        (p,q) = plus_proche(i,j,7,res)
                        (p,q) = plus_proche(p,q,2,res)
                        if (p,q) != (-1,-1):
                            res[p][q] = 0
                else:
                    (p,q) = plus_proche(i,j,2,res)
                    if (p,q) != (-1,-1):
                        res[p][q] = 0            
            if terrain[i][j] == 6:
                (p1,q1) = plus_proche(i,j,2,res)
                if (p1,q1) != (-1,-1):
                    res[p1][q1] = 0
                (p2,q2) = plus_proche(i,j,2,res)
                if (p2,q2) != (-1,-1):
                    res[p2][q2] = 0
                if distance(i,j,2,terrain,7) > 6:
                    if not distance(i,j,7,res,5) in [3,4,5]:
                        res[i][j] = 3
                    else:
                        (p1,q1) = plus_proche(i,j,7,res)
                        (p,q) = plus_proche(p1,q1,2,res)
                        if (p,q) != (-1,-1):
                            res[p][q] = 0
                        (p,q) = plus_proche(p1,q1,2,res)
                        if (p,q) != (-1,-1):
                            res[p][q] = 0
                else:
                    (p,q) = plus_proche(i,j,2,res)
                    if (p,q) != (-1,-1):
                        res[p][q] = 0
                    (p,q) = plus_proche(i,j,2,res)
                    if (p,q) != (-1,-1):
                        res[p][q] = 0
            if terrain[i][j] == 5 and res[i][j] == 5:
                res[i][j] = 0
    return res

def test_map(lar,lon,pts,N):
    for i in range(N):
        terrain = map_gen(lar,lon,pts)
        image = np.zeros((lar,lon,3),dtype=np.uint8)
        for k in range(len(terrain)):    
                for p in range(len(terrain[0])):
                    if terrain[k][p] == 0:
                        image[k,p] = (50,150,50)
                    elif terrain[k][p] == 1:
                        image[k,p] = (86,176,224)
                    elif terrain[k][p] == 2:
                        image[k,p] = (90,120,300)
                    elif terrain[k][p] == 3:
                        image[k,p] = (127,83,64)
        plt.imshow(image,interpolation = 'nearest')
        plt.axis('off')
        plt.show()
        time.sleep(0)
        plt.close()

def time_play(lar,lon,pts,pour):
    terrain = map_gen(lar,lon,pts)
    image = np.zeros((lar,lon,3),dtype=np.uint8)
    for k in range(len(terrain)):    
            for p in range(len(terrain[0])):
                if terrain[k][p] == 0:
                    image[k,p] = (50,150,50)
                elif terrain[k][p] == 1:
                    image[k,p] = (86,176,224)
                elif terrain[k][p] == 2:
                    image[k,p] = (90,120,300)
                elif terrain[k][p] == 3:
                    image[k,p] = (127,83,64)
    plt.figure(1)
    plt.imshow(image,interpolation = 'nearest')
    plt.axis('off')
    plt.show()
    while 1 > 0:
        time.sleep(0)
        terrain = evolve(terrain,pour)
        for k in range(len(terrain)):    
            for p in range(len(terrain[0])):
                if terrain[k][p] == 0:
                    image[k,p] = (50,150,50)
                elif terrain[k][p] == 1:
#                    if voisins(k,p,1,terrain) > 6:
#                        image[k,p] = (90,120,250)
#                    else:
#                        image[k,p] = (86,176,224)
                    image[k,p] = (86,176,224)
                elif terrain[k][p] == 2:
                    image[k,p] = (90,120,300)
                elif terrain[k][p] == 4:
                    image[k,p] = (162,170,36)
                elif terrain[k][p] == 5:
                    image[k,p] = (50,50,50)
                elif terrain[k][p] == 3:
                    image[k,p] = (127,83,64)
                elif terrain[k][p] == 6:
                    image[k,p] = (190,190,190)
                elif terrain[k][p] == 7:
                    image[k,p] = (152,81,42)
        plt.imshow(image,interpolation = 'nearest')
        plt.axis('off')
        plt.show()
        plt.close(1)

def time_play_bis(lar,lon,pts,pour):
    terrain = map_gen(lar,lon,pts)
    image = np.zeros((lar,lon,3),dtype=np.uint8)
    for k in range(len(terrain)):    
            for p in range(len(terrain[0])):
                if terrain[k][p] == 0:
                    image[k,p] = (50,150,50)
                elif terrain[k][p] == 1:
                    image[k,p] = (86,176,224)
                elif terrain[k][p] == 2:
                    image[k,p] = (90,120,300)
                elif terrain[k][p] == 3:
                    image[k,p] = (127,83,64)
    plt.imshow(image,interpolation = 'nearest')
    plt.axis('off')
    plt.show()
    while 1 > 0:
        terrain = evolve(terrain,pour)
        for k in range(len(terrain)):    
            for p in range(len(terrain[0])):
                if terrain[k][p] == 0:
                    image[k,p] = (50,150,50)
                elif terrain[k][p] == 1:
#                    if voisins(k,p,1,terrain) > 6:
#                        image[k,p] = (90,120,250)
#                    else:
#                        image[k,p] = (86,176,224)
                    image[k,p] = (86,176,224)
                elif terrain[k][p] == 2:
                    image[k,p] = (90,120,300)
                elif terrain[k][p] == 4:
                    image[k,p] = (162,170,36)
                elif terrain[k][p] == 5:
                    image[k,p] = (50,50,50)
                elif terrain[k][p] == 3:
                    image[k,p] = (127,83,64)
                elif terrain[k][p] == 6:
                    image[k,p] = (190,190,190)
                elif terrain[k][p] == 7:
                    image[k,p] = (152,81,42)
        plt.imshow(image,interpolation = 'nearest')
        plt.axis('off')
        plt.show()
        plt.close()

#test_map(100,100,[[30,12,60],[30,10,300],[8,30,100],25],20)
time_play(100,100,[[30,70,120],[40,10,300],[5,60,150],20],[150,500])


